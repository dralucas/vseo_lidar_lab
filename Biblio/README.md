# References

Here are some references, but many (many) other papers can be considered.


[Brodu & Lague, 2012](BroduLague_ISPRS.pdf) - 3D terrestrial lidar data classification of complex natural scenes using a multi-scale dimensionality criterion: Applications in geomorphology

[Duncanson et al., 2022](1-s2.0-S0034425721005654-main.pdf) - Aboveground biomass density models for NASA’s Global Ecosystem Dynamics Investigation (GEDI) lidar mission

[Lague, Brodu & Leroux, 2013](1-s2.0-S0924271613001184-main.pdf) - Accurate 3D comparison of complex topography with terrestrial laser
scanner: Application to the Rangitikei canyon (N-Z) 

[Luetzenburg, Kroon & Bjørk, 2021](s41598-021-01763-9.pdf) - Evaluation of the Apple iPhone
12 Pro LiDAR for an Application
in Geosciences

[Shen et al., 2023](679375.pdf) - FARMYARD: A Generic GPU-based Pipeline for Feature
Discovery from Massive Planetary LiDAR Data

[Tavania et al. 2022](1-s2.0-S0012825222000538-main.pdf) - Smartphone assisted fieldwork: Towards the digital transition of geoscience fieldwork using LiDAR-equipped iPhones

[Vazirabad & Karslioglu, 2011](19065.pdf) - Lidar for Biomass Estimation



