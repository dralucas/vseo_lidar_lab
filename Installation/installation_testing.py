try:
    # Import the required packages

    ## Standard packages
    import glob
    import os
    import numpy as np

    ## Graphics packages
    import matplotlib.pyplot as plt

    ## data packages
    import pandas as pd
    import pickle
    import json
    import pdal


    from multiprocessing import Pool
    from functools import partial

    import laspy
    import matplotlib.tri as tri

    import numpy as np
    from scipy.stats import gaussian_kde
    from scipy.spatial import KDTree

    ## TQDM for progress bars
    import tqdm

    ## Machine learning packages
    from sklearn.svm import SVC
    from sklearn.metrics import ConfusionMatrixDisplay
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.ensemble import GradientBoostingClassifier
    from sklearn.neighbors import KNeighborsClassifier
    from sklearn.model_selection import train_test_split

    from sklearn.metrics import classification_report

    ## Avoid FutureWarnings 
    print("  ")
    print("Good news, all required packages are installed!") 

    
except ImportError:
    print("  ")
    print("It seems that some packages are missing. Please install the required packages using either:")
    print("conda env create -f environment.yml")
    print("or")
    print("conda env update -f environment.yml")
    exit(1)