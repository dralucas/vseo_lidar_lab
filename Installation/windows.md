# Windows installer

## Native installation
To set up a similar environment on Windows, the process is mostly similar but requires a few adjustments due to differences in commands and paths between Linux and Windows. 

Here's how you can adapt your steps:


**<span style="color:red">/!\ Warning: The Windows installation has not been tested!</span>**


### Step 1: Download and Install Anaconda for Windows

   - Go to the [Anaconda Distribution](https://www.anaconda.com/products/distribution) page and download the installer for Windows.
   - Run the installer and follow the prompts. Make sure to check the option to add Anaconda to your PATH environment variable.

### Step 2: Initialize Conda
   - Open the **Anaconda Prompt** from the Start menu.

### Step 3: Create a New Conda Environment and Install Required Packages

```bash
# Create a new environment
ENV_NAME="vseo_lidar_lab"
conda create -n %ENV_NAME% -y

# Activate the environment
conda activate %ENV_NAME%

# Install necessary packages
conda install -c conda-forge numpy scipy matplotlib pandas jupyter scikit-learn ipywidgets seaborn jupyterlab nodejs jupyterlab_widgets -y
conda install -c conda-forge pdal python-pdal -y
# Install additional packages via pip
pip install tqdm laspy lazrs

# Initialize Conda (optional, depending on your setup)
conda init
```


### Additional Notes:
- **Anaconda Prompt**: On Windows, you'll typically use the Anaconda Prompt instead of a standard terminal or Bash shell. This prompt is pre-configured with Anaconda and Conda commands.
- **Environment Variables**: Windows uses `%VARIABLE_NAME%` instead of `$VARIABLE_NAME` for environment variables.
- **Paths**: On Windows, paths use backslashes (`\`) instead of forward slashes (`/`). The `%HOMEPATH%` environment variable typically corresponds to `C:\Users\YourUsername`, so `%HOMEPATH%\Desktop` is your desktop.

By following these steps, you should be able to replicate the setup for Linux/macOS on a Windows machine.

**<span style="color:red">/!\ Warning: The Windows installation has not been tested!</span>**


## WSL installation

Windows 10 and 11 come with a subsystem for Linux named WSL and WSL2. This allows you to run a Linux environment along side with Windows without the need of dualboot nor virtual machine. Once WSL is installed, the Linux installation workflow should work smoothly. 

# Installation testing

From the Anaconda prompt, you can test if your system is correctly setup by typing:

```bash
conda activate vseo_lidar_lab
python3 installation_testing.py
```

You can also try with the notebook `installation_testing.ipynb` using Jupyter-Lab or notebook.


