#!/bin/bash
clear
echo "
 ___      ___  ________       _______       ________      ___   ___                                            
|\  \    /  /||\   ____\     |\  ___ \     |\   __  \    |\  \ |\  \                                           
\ \  \  /  / /\ \  \___|_    \ \   __/|    \ \  \|\  \   \ \  \\_\  \                                          
 \ \  \/  / /  \ \_____  \    \ \  \_|/__   \ \  \\\  \   \ \______  \                                         
  \ \    / /    \|____|\  \    \ \  \_|\ \   \ \  \\\  \   \|_____|\  \                                        
   \ \__/ /       ____\_\  \    \ \_______\   \ \_______\         \ \__\                                       
    \|__|/       |\_________\    \|_______|    \|_______|          \|__|                                       
                 \|_________|       "                                                                           
                                                                                                                                                                                                                   
echo " "
echo " "                                                                                                                                                       
echo "This script will set up the VSEO4 LiDAR lab environment on your machine."
echo " "


# Step 1: Update and install dependencies
# sudo apt-get update -y
# sudo apt-get install -y wget git


# # Step 2: Download and install Anaconda for Linux
ANACONDA_INSTALLER=Anaconda3-2024.06-1-Linux-x86_64.sh
wget https://repo.anaconda.com/archive/$ANACONDA_INSTALLER
bash $ANACONDA_INSTALLER -b -p $HOME/anaconda3

# # Step 3: Initialize Conda
eval "$($HOME/anaconda3/bin/conda shell.bash hook)"


ENV_NAME="vseo_lidar_lab"
# Step 4: Create a new Conda environment and install the required packages
conda create -n $ENV_NAME -y
conda activate $ENV_NAME
conda install -c conda-forge numpy scipy matplotlib pandas jupyter scikit-learn ipywidgets seaborn jupyterlab  nodejs jupyterlab_widgets -y
pip install tqdm laspy lazrs
conda install -c conda-forge pdal python-pdal -y
$HOME/anaconda3/bin/conda init


# Step 5: Notify the user
echo "Setup is complete. The repository has been cloned to $DESKTOP_DIR. Activate the environment using 'conda activate $ENV_NAME' and navigate to the repository folder to start working on the notebooks."
echo " "
echo "Happy coding!"

# End of script