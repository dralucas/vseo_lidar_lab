# VSEO4 LiDAR Lab

This lab is part of the course Earth data science taught at the Institut de physique du globe de Paris, France. The labs are taught by [Antoine Lucas](http://dralucas.geophysx.org/), [Alexandre Fournier](https://www.ipgp.fr/~fournier/), [Éléonore Stutzmann](https://www.ipgp.fr/~stutz/) and [Léonard Seydoux](https://sites.google.com/view/leonard-seydoux/accueil).


## Python environment

To facilitate the lab, we are going to first setup an Anaconda environment with the following set of commands. 


Either you download the setup script named [setup_vseo4_lidar.sh](./setup_vseo4_lidar.sh) 
Then from the Terminal you run it as:

```bash
chmod +x setup_vseo4_lidar.sh
./setup_vseo4_lidar.sh
```

or from your a terminal you manually run the following commands:
```bash
ANACONDA_INSTALLER=Anaconda3-2024.06-1-Linux-x86_64.sh
wget https://repo.anaconda.com/archive/$ANACONDA_INSTALLER
bash $ANACONDA_INSTALLER -b -p $HOME/anaconda3
eval "$($HOME/anaconda3/bin/conda shell.bash hook)"
```
Then, let's create the appropriate environment. The following lines create a new environment called `vseo_lidar_lab` without any package installed. Then, we install the required packages.

```bash
conda create -n vseo_lidar_lab
conda activate vseo_lidar_lab
conda install -c conda-forge numpy scipy matplotlib pandas jupyter scikit-learn cartopy ipywidgets seaborn jupyterlab nodejs jupyterlab_widgets
conda install -c conda-forge pdal python-pdal
pip install tqdm 
pip install laspy lazrs
$HOME/anaconda3/bin/conda init
```

## Testing your installation

__Option: 1 Running the testing script__

``` bash
conda activate vseo_lidar_lab
python3 installation_testing.py
```

__Option: 2 Open the Notebook in JupyterLab__

If you want to open the notebook in the JupyterLab interface:

Activate your conda environment:
```bash
conda activate vseo_lidar_lab
jupyter-lab installation_testing.ipynb
```
Navigate to the notebook file (.ipynb) you want to run, and click to open it in the JupyterLab interface.
You must select the kernel `vseo_lidar_lab` in Jupyter to run the notebooks. Please inform your instructor if you have any problem with this.


__Option: 3 Run the Notebook in Jupyter Notebook Interface__

If you prefer using the classic Jupyter Notebook interface:

Activate your conda environment:

```bash
conda activate vseo_lidar_lab
jupyter notebook
```

Navigate to the notebook file (`installation_testing.ipynb`) and click to open it. Make sure you selected the kernel `vseo_lidar_lab` before running the notebook. 
