# LiDAR 3D cloud point segmentation using Machine Learning to estimate Biomass


This lab is part of the course Earth data science taught at the Institut de physique du globe de Paris, France. The labs are taught by [Antoine Lucas](http://dralucas.geophysx.org/), [Alexandre Fournier](https://www.ipgp.fr/~fournier/), [Éléonore Stutzmann](https://www.ipgp.fr/~stutz/) and [Léonard Seydoux](https://sites.google.com/view/leonard-seydoux/accueil).


## Python environment

See Installation instructions for getting ready
