# Pre-processing pipeline for extracting learning data from the raw LiDAR scene

## Selecting learning region of interest (ROI)

### 1. Converting the ground points to DEM

We use the `pdal` library to convert the ground points to a DEM. The pipeline is defined in the `laz2dem.json` file. The pipeline reads the raw LiDAR data and extracts the ground points. The ground points are then used to create a DEM, being a regular grid of elevation values. The DEM is saved in the `dem.tif` file as defined into the json file.

```bash
pdal pipeline laz2dem.json
```

### 2. Selecting the learning ROI

We use a GIS software to select the learning ROI. The ROI is defined by a polygon shapefile. The shapefile is saved in the `*.shp` files. We used QGIS to select the ROI.

### 3. Extracting the learning data

We use the `pdal` library to extract the learning data from the DEM and the shapefiles. First we extract the polygon coordinates from the shapefile using `ogr` library:

```bash
ogr2ogr -f "CSV" /vsistdout/ poly_build.shp  -lco GEOMETRY=AS_WKT
```
and we copy the output into the json file. Then we use the `pdal` library to extract the learning data. The pipeline is defined in the `crop_*.json` files.

```bash
pdal pipeline crop_road.json
```
We end up with the `*.laz` files containing the learning data as `*_subset.laz`.

