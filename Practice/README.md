# Dataset for ML based classification and post-analysis of LiDAR scans

For the sake of the VSEO4 practical part, we will use a point clouds acquired with a UAV LiDAR sensor near Tan Thanh in Vietnam. Other dataset are offered for those who want to explore the technique in various contexts.

![image](images/scene2.png)



## Tan Thanh dataset

Son Tong Si provided us UAV LiDAR scan that was acquired in a region close to Tan Thanh in Vietnam. We will investigate this dataset during the practical session of the VSEO4.

The directory of the dataset is located in `data/TanThanh_Vietnam`.




## Mont Saint-Michel dataset

This dataset contain a point cloud acquired by a terrestrial lidar sensor on a beach, and containing two classes: sand dunes and vegetation. This dataset is provided by Brodu and Lague ([2012](https://www.sciencedirect.com/science/article/abs/pii/S0924271612000330)).

The directory of the dataset is located in `data/MtStMichel_France`.






