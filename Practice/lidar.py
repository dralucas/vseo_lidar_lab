"""Read, process and plot lidar data.

The initial version of this module was written by Antoine Lucas and Grégory
Sainton. The code was updated by Alexandre Fournier with parallel processing
using the mpi4py library. The code was then cleaned up and modified by Leonard
Seydoux, and now uses the multiprocessing library and the "core points" trick to
speed up the feature extraction. The code was then adapted to the VSEO4 course by Antoine Lucas.

Authors:
    Léonard Seydoux, Grégory Sainton, Antoine Lucas, and Alexandre Fournier.

Last update:
    September 2024, Antoine Lucas (adapted to VSEO4 course).
"""

from multiprocessing import Pool
from functools import partial

import laspy
from matplotlib import pyplot as plt
import matplotlib.tri as tri

import numpy as np
from scipy.stats import gaussian_kde
from scipy.spatial import KDTree

import os

import tqdm

def read_xyz(filepath: str) -> np.ndarray:
    """Read xyz file.

    Read a lidar data file (.xyz) and return the points coordinates in meters.

    Parameters
    ----------
    filepath : str
        Full path to the file to read.

    Returns
    -------
    points : np.ndarray
        Array of points coordinates in meters, of shape (n_points, 3)
    """
    # Read file
    points = np.loadtxt(filepath)

    # Remove NaNs
    points = points[~np.any(np.isnan(points), axis=1)]

    return points[:, :3]


def read_las(filepath: str) -> np.ndarray:
    """Read LAS (for LASer) file.

    Read a lidar data file (.las, or .laz) and return the points coordinates in
    meters. This function is a wrapper around laspy.read() and the xyz
    attribute of the returned object.

    Parameters
    ----------
    filepath : str
        Full path to the file to read.

    Returns
    -------
    points : np.ndarray
        Array of points coordinates in meters, of shape (n_points, 3)
    """
    return laspy.read(filepath).xyz

#function to read binary PLY files
def read_ply(filepath: str) -> np.ndarray:
    """Read PLY (Polygon File Format) file.

    Read a lidar data file (.ply) and return the points coordinates in
    meters. This function is a wrapper around np.fromfile().

    Parameters
    ----------
    filepath : str
        Full path to the file to read.

    Returns
    -------
    points : np.ndarray
        Array of points coordinates in meters, of shape (n_points, 3)
    """
    # Read file
    points = np.fromfile(filepath, dtype=np.float32)
    points = points.reshape(-1, 3)
    return points


def save_las(points: np.ndarray, classifications: np.ndarray, filepath: str) -> None:
    """
    Save a 3D point cloud and classifications into a LAS file.

    Parameters
    ----------
    points : np.ndarray
        Array of point coordinates in meters, of shape (n_points, 3).
    classifications : np.ndarray
        Array of classification indices, of shape (n_points,).
    filepath : str
        Full path to save the LAS file.
    
    Raises
    ------
    ValueError
        If the number of points does not match the number of classifications.
    """
    if points.shape[0] != classifications.shape[0]:
        raise ValueError("The number of points must match the number of classifications.")
    
    # Create a new LAS header
    header = laspy.LasHeader(point_format=3, version="1.2")  # Version 1.2, Point Format 3 supports classification
    
    # Create a new LAS file with the header
    las = laspy.LasData(header)
    
    # Set the x, y, z coordinates
    las.x = points[:, 0]
    las.y = points[:, 1]
    las.z = points[:, 2]
    
    # Set the classification data
    las.classification = classifications
    
    # Write to the LAS file
    las.write(filepath)

def save_las_with_metadata(points: np.ndarray, classifications: np.ndarray, output_filepath: str, template_filepath: str) -> None:
    """
    Save a 3D point cloud and classifications into a LAS file, copying metadata
    (including CRS) from a template LAS/LAZ file.

    Parameters
    ----------
    points : np.ndarray
        Array of point coordinates in meters, of shape (n_points, 3).
    classifications : np.ndarray
        Array of classification indices, of shape (n_points,).
    output_filepath : str
        Full path to save the LAS file.
    template_filepath : str
        Full path to the template LAS/LAZ file from which to copy metadata (e.g. CRS).

    Raises
    ------
    ValueError
        If the number of points does not match the number of classifications.
    """
    if points.shape[0] != classifications.shape[0]:
        raise ValueError("The number of points must match the number of classifications.")
    
    # Read the template LAS/LAZ file for metadata
    template_las = laspy.read(template_filepath)
    
    # Create a new LAS header using the template file's header as a base
    header = laspy.LasHeader(point_format=template_las.header.point_format.id,
                             version=template_las.header.version)
    
    # Copy the template file's metadata into the new header
    header.offsets = template_las.header.offsets
    header.scales = template_las.header.scales
    header.min = template_las.header.min
    header.max = template_las.header.max
    
    # Create a new LAS file with the copied header
    las = laspy.LasData(header)
    
    # Set the x, y, z coordinates
    las.x = points[:, 0]
    las.y = points[:, 1]
    las.z = points[:, 2]
    
    # Set the classification data
    las.classification = classifications
    
    # Copy the VLRs (including CRS information) from the template file
    las.vlrs = template_las.vlrs  # Copies all VLRs, including CRS
    
    # Write to the LAS file
    las.write(output_filepath)


def sample(points: np.ndarray, n_points: int) -> np.ndarray:
    """Select a random sample of array elements.

    Given an array of points, select a random sample of n_points elements.
    The function works on the first axis of the array.

    Parameters
    ----------
    points : array-like
        Array of points coordinates in meters, of shape (3, n_points).
    n_points : int
        Number of points to sample.

    Returns
    -------
    points : array-like
        Array of points coordinates in meters, of shape (n_points, 3).
    """
    # Get number of points
    n_total = points.shape[0]

    # Get indices of sampled points
    indices = sample_indices(n_total, n_points)

    return points[indices]

import numpy as np

def gridPoints(x, y, z, step, agg_func=np.nanmax):
    """
    Bins scattered data and takes the aggregate (max/min) value to grid over a regular space domain.
    ----
    INPUT:
        x, y, z: Scattered field data.
        step: Required spatial sampling, expressed in the same unit as x and y.
        agg_func: Aggregation function to apply (e.g., np.nanmax for canopy or np.nanmin for ground).
    OUTPUT:
        Grid: Regular grid of the values from z according to bins in the space domain of minmax(x, y).
        X, Y: Coordinates of the output grid in the same unit as input x, y.
    """
    if step <= 0:
        raise ValueError("Step must be a positive number.")
    
    # Define grid limits and number of bins
    x_min, x_max = np.min(x), np.max(x)
    y_min, y_max = np.min(y), np.max(y)
    
    nx = int((x_max - x_min) / step) + 1
    ny = int((y_max - y_min) / step) + 1
    
    if nx < 2 or ny < 2:
        raise ValueError("Step size too large, resulting in an invalid grid.")
    
    # Generate grid coordinates
    X = np.linspace(x_min, x_max, num=nx)
    Y = np.linspace(y_min, y_max, num=ny)
    
    # Define bins
    x_bins = np.linspace(x_min, x_max, num=nx + 1)
    y_bins = np.linspace(y_min, y_max, num=ny + 1)
    
    # Use histogram2d to bin x and y coordinates
    H, _, _ = np.histogram2d(x, y, bins=[x_bins, y_bins])
    
    # Prepare output grid
    Grid = np.full((nx - 1, ny - 1), np.nan)
    
    # Iterate over each bin and apply aggregation function
    for xi in tqdm.tqdm(range(nx - 1), desc="Processing X-axis bins"):
        for yj in range(ny - 1):
            mask = (x >= X[xi]) & (x < X[xi + 1]) & (y >= Y[yj]) & (y < Y[yj + 1])
            if np.any(mask):
                Grid[xi, yj] = agg_func(z[mask])
    
    return Grid, X, Y

# Now filter the points based on classification and grid both canopy and ground
def generateCanopyGroundGrids(points, classifications, step):
    """
    Generate two aligned grids for canopy and ground.
    INPUT:
        points: Array of shape (n, 3) with columns [x, y, z].
        classifications: Array of point classifications.
        step: Grid resolution.
    OUTPUT:
        canopy_grid, ground_grid: Two aligned grids for canopy and ground heights.
        X, Y: Grid coordinates.
    """
    x, y, z = points[:, 0], points[:, 1], points[:, 2]
    
    # Separate canopy and ground points by classification
    canopy_idx = classifications == 3  # Classification 3 for canopy
    ground_idx = np.isin(classifications, [0, 1, 2])  # Classification 0, 1, and 2 for ground
    
    # Grid the canopy points (max height in each grid cell)
    canopy_grid, X, Y = gridPoints(x[canopy_idx], y[canopy_idx], z[canopy_idx], step, agg_func=np.nanmax)
    
    # Grid the ground points (min height in each grid cell)
    ground_grid, _, _ = gridPoints(x[ground_idx], y[ground_idx], z[ground_idx], step, agg_func=np.nanmin)
    
    return canopy_grid, ground_grid, X, Y

# Function to compute the canopy height difference
def computeCanopyHeight(canopy_grid, ground_grid):
    """
    Compute the canopy height difference between the canopy and ground grids.
    INPUT:
        canopy_grid, ground_grid: Aligned grids of canopy and ground heights.
    OUTPUT:
        Canopy height difference grid.
    """
    canopy_height = canopy_grid - ground_grid
    return canopy_height


def sample_indices(n_total: int, n_points: int) -> np.ndarray:
    """Select a random sample of array indices.

    Given an array of points, select a random sample of n_points elements.
    The function works on the first axis of the array.

    Parameters
    ----------
    n_total : int
        Total number of points.
    n_points : int
        Number of points to sample.

    Returns
    -------
    indexes : array-like
        Array of indexes, of shape (n_points,).
    """
    # Check that the number of points is not larger than the array size
    n_points = min(n_points, n_total)

    # Sample points
    indices = np.arange(n_total)
    indices = np.random.choice(indices, size=n_points)

    return indices

def set_axes_equal(ax):
    """Set 3D plot axes to have equal scale so that spheres appear as spheres,
    cubes as cubes, etc. This is meant to fix the aspect ratio in 3D plots.
    
    Parameters
    ----------
    ax : Axes3DSubplot
        A matplotlib 3D axis object.
    """
    limits = np.array([ax.get_xlim3d(), ax.get_ylim3d(), ax.get_zlim3d()])
    # Compute the span (range) for each axis
    spans = limits[:, 1] - limits[:, 0]
    # Get the midpoint of each axis
    centers = np.mean(limits, axis=1)
    # Set the max span
    max_span = max(spans)
    
    # Set the limits for each axis to center and have equal spans
    ax.set_xlim3d([centers[0] - max_span / 2, centers[0] + max_span / 2])
    ax.set_ylim3d([centers[1] - max_span / 2, centers[1] + max_span / 2])
    ax.set_zlim3d([centers[2] - max_span / 2, centers[2] + max_span / 2])


def plot_scene(points: np.ndarray, ax=None, n_points: int = None, **kwargs):
    """Show a 3D scene with points.

    Parameters
    ----------
    points: np.ndarray
        Points to plot.
    ax: Axes3DSubplot, optional
        Axes of the figure, by default None.
    sample : int, optional
        Number of points to plot, by default 1000.
    **kwargs : dict, optional
        Keyword arguments for matplotlib.pyplot.scatter.

    Returns
    -------
    ax : Axes3DSubplot
        Axes of the figure.
    """
    # Get axes
    ax = ax or plt.axes(projection="3d")

    # Default arguments
    kwargs.setdefault("rasterized", True)

    # Decimate points
    if n_points is not None:
        # Sample points
        indices = sample_indices(points.shape[0], n_points)

        # Decimate colors and sizes
        if "c" in kwargs and isinstance(kwargs["c"], np.ndarray):
            kwargs["c"] = kwargs["c"][indices]
        if "s" in kwargs and isinstance(kwargs["s"], np.ndarray):
            kwargs["s"] = kwargs["s"][indices]

        # Decimate points
        points = points[indices]

    # Plot points
    ax.scatter(*points.T, **kwargs)

    # Label axes
    ax.set_xlabel("x (m)")
    ax.set_ylabel("y (m)")
    ax.set_zlabel("z (m)")

    return ax


def plot_ternary(x, y, ax=None, **kwargs):
    """Plot a ternary diagram.

    Parameters
    ----------
    x : array
        Array of x coordinates.
    y : array
        Array of y coordinates.
    ax : Axes, optional
        Axes of the figure, by default None.
    sort : bool, optional
        Sort points by density, by default True.
    bins : int, optional
        Number of bins, by default 20.
    **kwargs : dict, optional
        Keyword arguments for matplotlib.pyplot.scatter.

    Returns
    -------
    ax : Axes
        Axes of the figure.
    """
    # Axes instance
    ax = ax or plt.gca()

    # Reject NaNs
    points = np.vstack((x, y))
    points = points[:, ~np.any(np.isnan(points), axis=0)]
    x, y = points

    # Estimate density with a Gaussian kernel density estimator
    try:
        density = gaussian_kde(points)(points)
    except:
        density = np.ones_like(x)
        

    if 'c' not in kwargs:
        kwargs['c'] = density
        
    kwargs.setdefault("rasterized", True)
    mappable = ax.scatter(x, y, s=0.5, **kwargs)

    if 'c' not in kwargs:
        # Colorbar
        plt.colorbar(
            mappable,
            orientation="horizontal",
            label="Density of points",
            shrink=0.5,
            pad=0.05,
        )

    # Triangular grid
    corners = np.array([[0, 0], [1, 0], [0.5, np.sqrt(3) * 0.5]])
    triangle = tri.Triangulation(corners[:, 0], corners[:, 1])
    refiner = tri.UniformTriRefiner(triangle)
    trimesh = refiner.refine_triangulation(subdiv=3)
    ax.triplot(trimesh, color="0.5", linestyle="-", linewidth=0.5, zorder=0)

    # Create outline frame
    ax.plot(
        np.hstack((corners[:, 0], corners[0, 0])),
        np.hstack((corners[:, 1], corners[0, 1])),
        color="k",
        linestyle="-",
        linewidth=plt.rcParams["axes.linewidth"],
        zorder=0,
    )

    # Labels
    ax.set_aspect("equal")
    ax.axis("off")
    ax.text(-0.015, -0.015, "1D", ha="right", va="top")
    ax.text(1.015, -0.015, "2D", ha="left", va="top")
    ax.text(0.5, np.sqrt(3) * 0.5 + 0.015, "3D", ha="center", va="bottom")

    return ax


def eigs(
    point: np.ndarray,
    points: np.ndarray = None,
    diameter: float = None,
    n_min_points: int = 10,
    tree: KDTree = None,
) -> np.ndarray:
    """Calculate the covariance matrix eigenvalues of a point neighborhood.

    Arguments:
    ----------
    point: array-like
        Point of interest of shape (3,).
    points: array-like
        Point cloud of shape (n, 3).
    diameter: float
        Diameter of the neighborhood.
    n_min_points: int, optional
        Minimum number of points in the neighborhood, by default 10.
    tree: KDTree, optional
        KDTree of the point cloud, by default None.

    Returns:
    --------
    covariance: array-like
        Covariance matrix of shape (3, 3).
    """
    # Calculate the distance to the point of interest
    neighbors = points[tree.query_ball_point(point, diameter / 2, workers=-1)]

    # Reject cases where the number of points is too small to calculate the
    # covariance matrix. In this case, return NaNs.
    if neighbors.shape[0] < n_min_points:
        return np.nan * np.ones(3)
    else:
        covariance = np.cov(neighbors, rowvar=False)
        eigenvalues = np.linalg.eigvalsh(covariance)[::-1]
        eigenvalues = eigenvalues / np.sum(eigenvalues)
        return eigenvalues


def calculate_eigenvalues(
    selected_points: np.ndarray,
    points: np.ndarray,
    diameter: float,
    n_min_points: int = 10,
) -> np.ndarray:
    """Calculate the eigenvalues of a point cloud.

    This function calculates the eigenvalues of the covariance matrix of a
    point cloud. The covariance matrix is calculated for each point in
    parallel.

    Arguments:
    ----------
    selected_points: array-like
        Points of interest of shape (m < n, 3).
    points: array-like
        Point cloud of shape (n, 3).
    diameter: float
        Diameter of the neighborhood.
    n_min_points: int, optional
        Minimum number of points in the neighborhood, by default 10.

    Returns:
    --------
    eigenvalues: array-like
        Eigenvalues of the covariance matrix.
    """
    # Get the tree of the point cloud
    tree = KDTree(points)

    # Define the function to calculate the covariance matrix of a point
    eigs_partial = partial(
        eigs,
        points=points,
        diameter=diameter,
        n_min_points=n_min_points,
        tree=tree,
    )

    num_processes = os.cpu_count()

    # Calculate the covariance in parallel
    # print if rank == 0 and num_processes > 1
    if num_processes > 1:
        if os.getenv("OMPI_COMM_WORLD_RANK") == "0":
            print("     Calculating covariance using", num_processes, "processes")

    with Pool(processes=num_processes) as pool:
        eigenvalues = pool.map(eigs_partial, selected_points)

    return np.array(eigenvalues)


def calculate_barycentric_coordinates(eigenvalues):
    """Calculate barycentric coordinates from eigenvalues.

    The barycentric coordinates are calculated from the eigenvalues of the
    covariance matrix of a point cloud. The eigenvalues are assumed to be
    normalized (i.e. they sum to 1). The barycentric coordinates are calculated as follows:

    .. math::

        x = \\frac{1}{2} \\frac{2b + c}{a + b + c}
        y = \\frac{\\sqrt{3}}{2} \\frac{c}{a + b + c}

    where :math:`a`, :math:`b`, and :math:`c` are the eigenvalues of the
    covariance matrix, and :math:`a \\geq b \\geq c`.

    Parameters
    ----------
    eigenvalues : array
        Array of eigenvalues.

    Returns
    -------
    tertiary : array
        Array of tertiary coordinates.
    """
    delta = np.sqrt(2) / 2 * (eigenvalues[:, 0] - eigenvalues[:, 1])
    Delta = np.sqrt(2) / 2
    c = eigenvalues[:, 2] / (1 / 3)
    a = (1 - c) * delta / Delta
    b = 1 - (a + c)
    x = 1 / 2 * (2 * b + c) / (a + b + c)
    y = np.sqrt(3) / 2 * c / (a + b + c)
    return np.vstack((x, y))